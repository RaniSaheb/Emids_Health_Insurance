package test.java.com.emids.health.insurance.testCases;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import main.java.com.emids.health.insurance.beans.CurrentHealth;
import main.java.com.emids.health.insurance.beans.Habits;
import main.java.com.emids.health.insurance.beans.User;
import main.java.com.emids.health.insurance.service.HealthInsuranceServiceImpl;

public class TestInsurancePremium {

		/*Name: Norman Gomes		Gender: Male		Age: 34 years
		Current health:		Hypertension: No  Blood pressure: No	Blood sugar: No 		Overweight: Yes
		Habits:		Smoking: No		Alcohol: Yes		Daily exercise: Yes		Drugs: No
		*/
	
	@Test
	public void testWorstCondition() {
		HealthInsuranceServiceImpl healthInsuranceServiceImpl4 =new HealthInsuranceServiceImpl();
		CurrentHealth curHealth4=new CurrentHealth();
		Habits habits4=new Habits();
		
		curHealth4.setBloodpressure(true);
		curHealth4.setBloodSugar(true);
		curHealth4.setHypertension(true);
		curHealth4.setOverWeight(true);
		
		habits4.setAlcohol(true);
		habits4.setDailyExercise(true);
		habits4.setDrug(true);
		habits4.setSmoking(true);
		
		User user4=new User("Ram Uday Singh","Male",60,curHealth4,habits4);
		int result4 =healthInsuranceServiceImpl4.calculatePremium(user4);
		assertEquals(16596,result4);
		
	}
	
	@Test
		public void testAll() {
			HealthInsuranceServiceImpl healthInsuranceServiceImpl =new HealthInsuranceServiceImpl();
			CurrentHealth curHealth=new CurrentHealth();
			Habits habits=new Habits();
			
			curHealth.setBloodpressure(false);
			curHealth.setBloodSugar(false);
			curHealth.setHypertension(false);
			curHealth.setOverWeight(true);
			
			habits.setAlcohol(true);
			habits.setDailyExercise(true);
			habits.setDrug(false);
			habits.setSmoking(false);
			
			User user=new User("Norman Gomes","Male",34,curHealth,habits);
			int finalCalculation =healthInsuranceServiceImpl.calculatePremium(user);
		    
			assertEquals(6856,finalCalculation);
			
		}
		
		@Test
		public void testAge() {
			HealthInsuranceServiceImpl healthInsuranceServiceImpl1 =new HealthInsuranceServiceImpl();
			CurrentHealth curHealth1=new CurrentHealth();
			Habits habits1=new Habits();
			
			curHealth1.setBloodpressure(false);
			curHealth1.setBloodSugar(false);
			curHealth1.setHypertension(false);
			curHealth1.setOverWeight(false);
			
			habits1.setAlcohol(false);
			habits1.setDailyExercise(false);
			habits1.setDrug(false);
			habits1.setSmoking(false);
			
			User user1=new User("Ranak Uday Singh","Other",17,curHealth1,habits1);
			int result1 =healthInsuranceServiceImpl1.calculatePremium(user1);
			assertEquals(5000,result1);
			
		}
		
		@Test
		public void testNormalCondition() {
			HealthInsuranceServiceImpl healthInsuranceServiceImpl2 =new HealthInsuranceServiceImpl();
			CurrentHealth curHealth2=new CurrentHealth();
			Habits habits2=new Habits();
			
			curHealth2.setBloodpressure(false);
			curHealth2.setBloodSugar(false);
			curHealth2.setHypertension(false);
			curHealth2.setOverWeight(false);
			
			habits2.setAlcohol(false);
			habits2.setDailyExercise(false);
			habits2.setDrug(false);
			habits2.setSmoking(false);
			
			User user2=new User("Rani Saheb","Female",50,curHealth2,habits2);
			int result2 =healthInsuranceServiceImpl2.calculatePremium(user2);
			assertEquals(10542,result2);
			
		}
		
		@Test
		public void testNegativeHabitCondition() {
			HealthInsuranceServiceImpl healthInsuranceServiceImpl3 =new HealthInsuranceServiceImpl();
			CurrentHealth curHealth3=new CurrentHealth();
			Habits habits3=new Habits();
			
			curHealth3.setBloodpressure(false);
			curHealth3.setBloodSugar(false);
			curHealth3.setHypertension(false);
			curHealth3.setOverWeight(false);
			
			habits3.setAlcohol(false);
			habits3.setDailyExercise(true);
			habits3.setDrug(false);
			habits3.setSmoking(false);
			
			User user3=new User("Rani Saheb","Female",50,curHealth3,habits3);
			int result3 =healthInsuranceServiceImpl3.calculatePremium(user3);
			assertEquals(10226,result3);
			
		}
		
		
		
		
		@Test
		public void testBadCondition() {
			HealthInsuranceServiceImpl healthInsuranceServiceImpl5 =new HealthInsuranceServiceImpl();
			CurrentHealth curHealth5=new CurrentHealth();
			Habits habits5=new Habits();
			
			curHealth5.setBloodpressure(true);
			curHealth5.setBloodSugar(false);
			curHealth5.setHypertension(true);
			curHealth5.setOverWeight(false);
			
			habits5.setAlcohol(false);
			habits5.setDailyExercise(true);
			habits5.setDrug(false);
			habits5.setSmoking(true);
			
			User user5=new User("Sulekha","Other",18,curHealth5,habits5);
			int result5 =healthInsuranceServiceImpl5.calculatePremium(user5);
			assertEquals(5101,result5);
			
		}
		
		
	}
