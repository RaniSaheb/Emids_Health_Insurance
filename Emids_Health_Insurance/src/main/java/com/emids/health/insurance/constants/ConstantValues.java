package main.java.com.emids.health.insurance.constants;

public class ConstantValues {

		public static final String MESSAGE ="Health Insurance Premium for  ";
		public static final int EIGHTEEN_TWENTYFIVE = 1;
		public static final int TWENTYFIVE_THIRTY = 2;
		public static final int THIRTY_THIRTYFIVE = 3;
		public static final int THIRTYFIVE_FOURTY = 4;
		public static final int FOURTY_FOURTYFIVE = 5;
		public static final int FOURTYFIVE_FIFTY = 6;
		public static final int FIFTY_FIFTYFIVE = 7;
		public static final int FIFTYFIVE_SIXTY = 8;
	}
