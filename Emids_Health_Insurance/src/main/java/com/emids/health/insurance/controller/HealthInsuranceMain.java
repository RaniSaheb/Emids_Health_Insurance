package main.java.com.emids.health.insurance.controller;

import java.util.Scanner;

import main.java.com.emids.health.insurance.beans.CurrentHealth;
import main.java.com.emids.health.insurance.beans.Habits;
import main.java.com.emids.health.insurance.beans.User;
import main.java.com.emids.health.insurance.constants.ConstantValues;
import main.java.com.emids.health.insurance.service.HealthInsuranceServiceImpl;

public class HealthInsuranceMain {
	 
		public static void main(String[] args) {
			//read values from console.
			HealthInsuranceMain hm=new HealthInsuranceMain();
			hm.readValuesFromscanner();
		}

		//variables required from scanner or scanner.
		 boolean bloodpressureCond=false;
		 boolean hypertensionCond=false;
		 boolean bloodSugarCond=false;
		 boolean overWeightCond=false;
		 boolean alcoholHabit=false;
		 boolean dailyExerciseHabit=false;
		 boolean drugHabit=false;
		 boolean smokingHabit=false;
		 String userName;
		 int age ;
		 String gender;
		 String bloodpressure ;
		 String bloodSugar;
		 String hypertension;
		 String overWeight;
		 String alcohol;
		 String dailyExercise ;
		 String drug;
		 String smoking;

		private  void readValuesFromscanner() {
			// create a Scanner so we can read input
				@SuppressWarnings("resource")
				Scanner scanner = new Scanner(System.in);
		    	 //  prompt for the user's name
				    System.out.print("Enter your name: ");
				    // get their input as a String
				    userName = scanner.next();
				    // prompt for their age
				    System.out.print("Enter your age: ");
				    // get the age as an int
				    age = scanner.nextInt();
				    //  prompt for the user's gender
				    System.out.print("Enter your gender: ");
				    // get their input as a String
				    gender = scanner.next();
				    //  prompt for the user's current health conditions
				    System.out.print("Current health conditions.");
				    System.out.println("Enter 'Yes' or 'No' for Bloodpressure: ");
				    bloodpressure = scanner.next();
				    System.out.print("Enter 'Yes' or 'No'  for BloodSugar: ");
				    bloodSugar = scanner.next();
				    System.out.print("Enter 'Yes' or 'No'  for Hypertension: ");
				    hypertension = scanner.next();
				    System.out.print("Enter 'Yes' or 'No'  for OverWeight: ");
				    overWeight = scanner.next();
				    
				    //  prompt for the user's current habits.
				    System.out.print("Current Habits.");
				    System.out.println("Enter 'Yes' or 'No' for Alcohol: ");
				    alcohol = scanner.next();
				    System.out.print("Enter 'Yes' or 'No'  for DailyExercise: ");
				    dailyExercise = scanner.next();
				    System.out.print("Enter 'Yes' or 'No'  for Drug: ");
				    drug = scanner.next();
				    System.out.print("Enter 'Yes' or 'No'  for Smoking: ");
				    smoking = scanner.next();
				    
		   
			HealthInsuranceServiceImpl healthInsuranceServiceImpl=new HealthInsuranceServiceImpl();
			
			//Creating the object of bean and setting all the values for CurrentHealth
			CurrentHealth curHealth7=new CurrentHealth();
			if(bloodpressure.equalsIgnoreCase("Yes")){
				bloodpressureCond=true;
			}if(bloodSugar.equalsIgnoreCase("Yes")){
				bloodSugarCond=true;
			}if(hypertension.equalsIgnoreCase("Yes")){
				hypertensionCond=true;
			}if(overWeight.equalsIgnoreCase("Yes")){
				overWeightCond=true;
			}
			curHealth7.setBloodpressure(bloodpressureCond);
			curHealth7.setBloodSugar(bloodSugarCond);
			curHealth7.setHypertension(hypertensionCond);
			curHealth7.setOverWeight(overWeightCond);
			
			
			//Creating the object of bean and setting all the values for Habits
			Habits habits7=new Habits();
			if(alcohol.equalsIgnoreCase("Yes")){
				alcoholHabit=true;
			}if(dailyExercise.equalsIgnoreCase("Yes")){
				dailyExerciseHabit=true;
			}if(drug.equalsIgnoreCase("Yes")){
				drugHabit=true;
			}if(smoking.equalsIgnoreCase("Yes")){
				smokingHabit=true;
			}
			habits7.setAlcohol(alcoholHabit);
			habits7.setDailyExercise(dailyExerciseHabit);
			habits7.setDrug(drugHabit);
			habits7.setSmoking(smokingHabit);
			
			
			//Initializing the constructor for all the input values.
			User user7=new User(userName,gender,age,curHealth7,habits7);
			
			//Calling service method for calculation of Insurance Premium.
			int finalCalculation=healthInsuranceServiceImpl.calculatePremium(user7);
		    
			//Printing the values to console.
			System.out.println(ConstantValues.MESSAGE+ " "+user7.getName()+" : Rs." +finalCalculation);
		}

		
	}
