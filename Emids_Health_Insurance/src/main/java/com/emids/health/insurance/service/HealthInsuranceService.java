package main.java.com.emids.health.insurance.service;

import main.java.com.emids.health.insurance.beans.User;

public interface HealthInsuranceService {

		public int calculatePremium(User user);
	}
