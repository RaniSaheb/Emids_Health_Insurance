package main.java.com.emids.health.insurance.service;

import main.java.com.emids.health.insurance.beans.User;
import main.java.com.emids.health.insurance.constants.ConstantValues;

public class HealthInsuranceServiceImpl  implements HealthInsuranceService {
	
	//Health Insurance Premium for Mr. Gomes: Rs. 6,856
	//Base premium for anyone below the age of 18 years = Rs. 5,000
	static double basicAmount=5000;
	static double finalCalculation = 0;
	static int finalPercentageToCalculate=0;
	
	@Override
		public int calculatePremium(User user) {
		finalCalculation=basicAmount;
		finalCalculation =calculateRanges(user);
			
			//Gender rule: Male vs female vs Other % -> Increase 2% over standard slab for Males
			if(user.getGender().equalsIgnoreCase("Male")) {
				finalCalculation = finalCalculation+(finalCalculation*2)/100;
			}
			
			//calculation based on Pre-existing conditions (Hypertension | Blook pressure | Blood sugar | Overweight) 
			//-> Increase of 1% per condition 
			if(user.getCurrentHealth().isHypertension()) {
				finalCalculation=finalCalculation+(finalCalculation*1)/100;
			} if(user.getCurrentHealth().isBloodpressure()) {
				finalCalculation=finalCalculation+(finalCalculation*1)/100;
			} if(user.getCurrentHealth().isOverWeight()) {
				finalCalculation=finalCalculation+(finalCalculation*1)/100;
			} if(user.getCurrentHealth().isBloodSugar()) {
				finalCalculation=finalCalculation+(finalCalculation*1)/100;
			}
			
			
			//check the percentage for habits to operate.
		calculateGoodAndBadHabits(user);
		if(finalPercentageToCalculate<0){
			finalCalculation=finalCalculation+(finalCalculation*finalPercentageToCalculate)/100;
		}
		if(finalPercentageToCalculate>0){
			finalCalculation=finalCalculation+(finalCalculation*finalPercentageToCalculate)/100;
		}
		int result =(int) Math.ceil( finalCalculation);
		
			return result;
				
		
		}

		public int calculateGoodAndBadHabits(User user){
			/*Habits
			Good habits (Daily exercise) -> Reduce 3% for every good habit
			Bad habits (Smoking | Consumption of alcohol | Drugs) -> Increase 3% for every bad habit*/
			
			if(user.getHabits().isAlcohol()){
				finalPercentageToCalculate+=3;
			}
			if(user.getHabits().isDailyExercise()){
				finalPercentageToCalculate-=3;
			}
			if(user.getHabits().isDrug()){
				finalPercentageToCalculate+=3;
			}
			if(user.getHabits().isSmoking()){
				finalPercentageToCalculate+=3;
			}
			
			
			return finalPercentageToCalculate;
		}
		
		public  int range(int num){ 
			/*% increase based on age: 18-25 -> + 10% | 25-30 -> +10% | 30-35 -> +10% 
			| 35-40 -> +10% | 40+ -> 20% increase every 5 years*/
		    if ( 18 < num && num <= 25)
		        return 1;
		    if ( 25 < num && num <= 30)
		        return 2;
		    if ( 30 < num && num <= 35)
		        return 3;
		    if ( 35 < num && num <= 40)
		        return 4;
		    if ( 40 < num && num <= 45)
		        return 5;
		    if ( 45 < num && num <= 50)
		        return 6;
		    if ( 50 < num && num <= 55)
		        return 7;
		    if ( 55 < num && num <= 65)
		        return 8;
			return 0;
		   
		}
		public double calculateRanges(User user){
			/*% increase based on age: 18-25 -> + 10% | 25-30 -> +10% | 30-35 -> +10% 
			| 35-40 -> +10% | 40+ -> 20% increase every 5 years*/
			//final int LESS_THAN_SEVENTEEN = 0;
			
			    switch (range(user.getAge())){
			        case ConstantValues.FIFTYFIVE_SIXTY: 
			        	finalCalculation = finalCalculation+(finalCalculation*20)/100;
			        case ConstantValues.FIFTY_FIFTYFIVE: 
			        	finalCalculation = finalCalculation+(finalCalculation*20)/100;
			        case ConstantValues.FOURTYFIVE_FIFTY: 
			        	finalCalculation = finalCalculation+(finalCalculation*20)/100;
			        case ConstantValues.FOURTY_FOURTYFIVE: 
			        	finalCalculation = finalCalculation+(finalCalculation*20)/100;
			        case ConstantValues.THIRTYFIVE_FOURTY: 
			        	finalCalculation = finalCalculation+(finalCalculation*10)/100;
			        case ConstantValues.THIRTY_THIRTYFIVE: 
			        	finalCalculation = finalCalculation+(finalCalculation*10)/100;
			        case ConstantValues.TWENTYFIVE_THIRTY: 
			        	finalCalculation = finalCalculation+(finalCalculation*10)/100;
			        case ConstantValues.EIGHTEEN_TWENTYFIVE: 
			        	finalCalculation = finalCalculation+(finalCalculation*10)/100;
			        	break;
			        default: 
			        	break;
			}
			    return finalCalculation;
		}
		
	}
